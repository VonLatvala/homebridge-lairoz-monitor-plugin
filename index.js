var Service, Characteristic;
const request = require('request');
const url = require('url');

module.exports = function (homebridge) {
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;
  homebridge.registerAccessory("win7displaypower-plugin", "LAIROZ-MONITOR", LAIROZ_MONITOR);
};

function LAIROZ_MONITOR(log, config) {
  this.log = log;
  this.service = "Switch";
  this.name = config['name'];
  this.getUrl = url.parse(config['getUrl']).href;
  this.postUrl = url.parse(config['postUrl']).href;
}

LAIROZ_MONITOR.prototype = {
  getServices: function () {
    const me = this;
    let informationService = new Service.AccessoryInformation();
    informationService
      .setCharacteristic(Characteristic.Manufacturer, "Axel Latvala")
      .setCharacteristic(Characteristic.Model, "win7displaypower-plugin")
      .setCharacteristic(Characteristic.SerialNumber, "133-950-001");

    let switchService = new Service.Lightbulb(me.name);
    switchService
      .getCharacteristic(Characteristic.On)
        .on('get', this.getSwitchOnCharacteristic.bind(this))
        .on('set', this.setSwitchOnCharacteristic.bind(this));

    this.informationService = informationService;
    this.switchService = switchService;
    return [informationService, switchService];
  },
  getSwitchOnCharacteristic: function (next) {
    const me = this;
    request({
        url: me.getUrl,
        method: 'GET',
    },
    function (error, response, body) {
      if (error) {
        if(!response) {
          me.log('STATUS: ENDPOINT UNAVAILABLE');
        } else {
            me.log('STATUS: ' + (response.statusCode || 'ENDPOINT UNAVAILABLE'));
        }
        me.log(error.message || 'Unknown');
        return next(error);
      }
      return next(null, JSON.parse(body).power);
    });
  },
  setSwitchOnCharacteristic: function (on, next) {
    const me = this;
    request({
      url: me.postUrl+'?set='+(on == true ? 'on' : 'off'),
      body: null,
      method: 'POST',
      headers: {'Content-type': 'application/json'}
    },
    function (error, response) {
      if (error) {
        if(!response) {
          me.log('STATUS: ENDPOINT UNAVAILABLE');
        } else {
            me.log('STATUS: ' + (response.statusCode || 'ENDPOINT UNAVAILABLE'));
        }
        me.log(error.message || 'Unknown');
        return next(error);
      }
      return next();
    });
  }
};
